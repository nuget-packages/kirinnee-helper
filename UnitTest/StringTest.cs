using System;
using Xunit;
using Kirinnee.Helper;
using System.Linq;

namespace test
{
    public class StringTest
    {
        [Fact]
        public void String_Reverse_should_reverse_the_string()
        {
            Assert.Equal("eennirik", "kirinnee".Reverse());
        }

        [Fact]
        public void SplitBy_should_split_string_into_string_enumerable()
        {
            string[] expected = {"a", "b", "c", "d"};

            Assert.Equal(expected, "a=>b=>c=>d".SplitBy("=>"));
        }

        [Fact]
        public void JoinBy_should_join_string_array_into_a_single_string()
        {
            const string expected = "a=>b=>c=>d";
            string[] test = {"a", "b", "c", "d"};
            Assert.Equal(expected, test.JoinBy("=>"));
        }

        [Fact]
        public void Take_should_take_first_x_character_of_string()
        {
            const string expected = "Sing";
            const string test = "Singapore";
            Assert.Equal(expected, test.Take(4));
        }


        [Fact]
        public void Take_should_take_all_character_if_it_exceeds_max_length_of_string()
        {
            const string expected = "Singapore";
            const string test = "Singapore";
            Assert.Equal(expected, test.Take(50));
        }

        [Fact]
        public void Skip_should_skip_first_x_character_of_string()
        {
            const string expected = "pore";
            const string test = "Singapore";
            Assert.Equal(expected, test.Skip(5));
        }

        [Fact]
        public void Skip_should_return_empty_string_if_it_exceeds_max_length_of_string()
        {
            const string expected = "";
            const string test = "Singapore";
            Assert.Equal(expected, test.Skip(100));
        }

        [Fact]
        public void Omit_should_omit_last_x_character_of_string()
        {
            const string expected = "Sing";
            const string test = "Singapore";
            Assert.Equal(expected, test.Omit(5));
        }

        [Fact]
        public void Omit_should_return_empty_string_if_it_exceeds_max_length_of_string()
        {
            const string expected = "";
            const string test = "Singapore";
            Assert.Equal(expected, test.Omit(100));
        }

        [Fact]
        public void Last_should_take_last_x_character_of_string()
        {
            const string expected = "pore";
            const string test = "Singapore";
            Assert.Equal(expected, test.Last(4));
        }

        [Fact]
        public void Last_should_return_all_characters_if_it_exceeds_max_length_of_string()
        {
            const string expected = "Singapore";
            const string test = "Singapore";
            Assert.Equal(expected, test.Last(100));
        }

        [Fact]
        public void ToInt_should_parse_numbers_to_int()
        {
            Assert.Equal(5, "5.5".ToInt());
            Assert.Equal(55, "55".ToInt());
            Assert.Equal(-55, "-55".ToInt());
        }

        [Fact]
        public void ToFloat_should_parse_number_to_float()
        {
            Assert.Equal(5.5, "5.5".ToFloat());
            Assert.Equal(-50.561, "-50.561".ToFloat(), 2);
        }

        [Fact]
        public void ToDouble_should_parse_number_to_double()
        {
            Assert.Equal(77.6849, "77.6849".ToDouble());
            Assert.Equal(-9922.1823, "-9922.1823".ToDouble());
        }

        [Fact]
        public void ToLong_should_parse_number_to_long()
        {
            Assert.Equal(3123123123123123123, "3123123123123123123".ToLong());
        }

        [Fact]
        public void Repeat_should_repeat_the_string_by_set_amount()
        {
            Assert.Equal("ababab", "ab".Repeat(3));
        }
    }
}