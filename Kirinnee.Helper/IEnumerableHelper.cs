﻿using System.Collections.Generic;
using System.Linq;

namespace Kirinnee.Helper
{
    public static class EnumerableHelper
    {
        public static IEnumerable<T> Omit<T>(this IEnumerable<T> x, int n)
        {
            return x.Reverse().Skip(n).Reverse();
        }

        public static IEnumerable<T> Last<T>(this IEnumerable<T> x, int n)
        {
            return x.Reverse().Take(n).Reverse();
        }

        public static IDictionary<T, int> Occurrence<T>(this IEnumerable<T> x)
        {
            var frequency = new Dictionary<T, int>();
            foreach(var  t in x)
            {
                if (frequency.ContainsKey(t))
                {
                    frequency[t]++;
                }
                else
                {
                    frequency[t] = 1;
                }
            }
            return frequency;
        }
    }
}
