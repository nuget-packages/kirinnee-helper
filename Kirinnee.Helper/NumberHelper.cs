using System;
using System.Collections;
using System.Collections.Generic;

namespace Kirinnee.Helper
{
    public static class NumberHelper
    {
        public static int ToInt(this float f)
        {
            return (int) f;
        }

        public static long ToLong(this float f)
        {
            return (long) f;
        }

        public static int ToInt(this double d)
        {
            return (int) d;
        }

        public static long ToLong(this double d)
        {
            return (long) d;
        }

        public static IEnumerable<T> Loop<T>(this int x, Func<int, T> lambda)
        {
            IList<T> y = new List<T>();
            for (var i = 0; i < x; i++)
            {
                y.Add(lambda(i));
            }
            return y;
        }
        
        public static int Floor(this float x)
        {
            return Math.Floor(x).ToInt();
        }

        public static int Ceil(this float x)
        {
            return Math.Ceiling(x).ToInt();
        }

        public static int Floor(this double x)
        {
            return Math.Floor(x).ToInt();
        }

        public static int Ceil(this double x)
        {
            return Math.Ceiling(x).ToInt();
        }

        public static double Root(this double x)
        {
            return Math.Sqrt(x);
        }

        public static double Root(this float x)
        {
            return Math.Sqrt(x);
        }

        public static double Root(this int x)
        {
            return Math.Sqrt(x);
        }

        public static double Root(this long x)
        {
            return Math.Sqrt(x);
        }
    }
}