﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Kirinnee.Helper
{
    public static class StringHelper
    {
        public static IEnumerable<string> SplitBy(this string x, string delimiter = "")
        {
            return delimiter == ""
                ? x.ToCharArray().Select(s => s.ToString()).ToArray()
                : x.Split(new string[] {delimiter}, StringSplitOptions.None);
        }

        public static string JoinBy(this IEnumerable<string> x, string delimiter = "")
        {
            return string.Join(delimiter, x);
        }

        public static string Reverse(this string x)
        {
            return x.SplitBy().Reverse().JoinBy();
        }


        public static string Take(this string x, int n)
        {
            return x.SplitBy().Take(n).JoinBy();
        }

        public static string Skip(this string x, int n)
        {
            return x.SplitBy().Skip(n).JoinBy();
        }

        public static string Omit(this string x, int n)
        {
            return x.SplitBy().Omit(n).JoinBy();
        }

        public static string Last(this string x, int n)
        {
            return x.SplitBy().Last(n).JoinBy();
        }


        public static int ToInt(this string x)
        {
            return Math.Floor(float.Parse(x)).ToInt();
        }

        public static float ToFloat(this string x)
        {
            return float.Parse(x);
        }

        public static double ToDouble(this string x)
        {
            return double.Parse(x);
        }

        public static long ToLong(this string x)
        {
            return long.Parse(x);
        }

        public static string Repeat(this string x, int n)
        {
            return n.Loop(i => x).JoinBy("");
        }


    }
}